# FCS-CCP-Analysis

FCS-CCP Library intended to access database (harricana.cs.mcgill.ca) and provide the some functionality to analyse the data collected from Even Online.

## Installation (for administrator)

Since the repository is private please use the following installation strategy (make sure that you have an access to it first):

```bash
pip install -e git+https://bitbucket.org/AlexanderButyaev/fcs-cpp-analysis.git#egg=fcs_ccp_analysis
```

## Dependencies (for administrator)

The library depends on the .env file that consists of

```
POSTGRESQL_DATABASE_URL=postgresql://localhost/fcs_ccp_dev
PROCESSED_FOLDER=/path/to/processed_folder
SOURCE_FOLDER=/path/to/source_folder
EXPERIMENT_FOLDER=/path/to/experiment_folder
RESULT_FOLDER=/path/to/result_folder
EXPORT_FOLDER=/path/to/export_folder
FCM_EXPORT_FOLDER=/path/to/fcm_export_folder
BATCH_EXPORT_FOLDER=/path/to/batch_export_folder
READY_TO_UPLOAD_FOLDER=/path/to/ready_to_upload_folder

PROCESSED_FOLDER__BK=/netdata/media/processed_folder
SOURCE_FOLDER__BK=/netdata/media/source_folder
EXPERIMENT_FOLDER__BK=/netdata/media/experiment_folder
RESULT_FOLDER__BK=/netdata/media/result_folder
EXPORT_FOLDER__BK=/netdata/media/export_folder
BATCH_EXPORT_FOLDER__BK=/netdata/media/batch_export_folder
READY_TO_UPLOAD_FOLDER__BK=/netdata/media/ready_to_upload_folder
GOLDS_FOLDER__BK=/netdata/media/golds
FCM_EXPORT_FOLDER__BK=/netdata/media/fcm_export_folder
```

## Jump start (not for admin)

If you were able to login castor.cs.mcgill.ca (as transmission) - all the environments and libraries already preinstalled there. To start working - execute the following:

```bash
source ~/.profile_project
source ~/virtualenvs/venv/bin/activate
python
```

And here is simple stuff to try if it works:

```python
# load the environment variable (unless it's done automatically)
from dotenv import load_dotenv
load_dotenv(".env")

# test if the environment variable is loaded
import os
print(os.getenv("READY_TO_UPLOAD_FOLDER"))

# import analysis stuff
from fcs_ccp_analysis import db, models

session = db.get_session()

batches = session.query(
	models.Batch
).with_entities(
	models.Batch.id,
	models.Batch.task_count
).all()

# print batches
for batch in batches:
	print(batch)

# ALWAYS CLOSE SESSION (it takes out the locks if any)
db.close()
```

## Extract datasets
```python
import os
from fcs_ccp_analysis import db, models

session = db.get_session()

files = session.query(
	models.File
).filter(
	# tag is a batch name without id at the end - for better aggregation
	models.File.tag == 'FR-FCM-Z2KP' # one of the most recent ones
).all()

dataset_paths = []
for f in files:
	path = f.get_file_path() + '.npy'
	if not os.path.exists(path):
		path = f.get_file_path__bk() + '.npy'

print(dataset_paths[0], os.path.exists(dataset_paths[0]))

# ALWAYS CLOSE SESSION (it takes out the locks if any)
db.close()
```

## Process solutions
```python
import os, json
import numpy as np # repo does not require it - install it yourself
from fcs_ccp_analysis import db, models, helpers

session = db.get_session()

solutions = session.query(
	models.File
).filter(
	# tag is a batch name without id at the end - for better aggregation
	models.File.tag == 'FR-FCM-Z2KP' # one of the most recent ones
).join(
	models.Solution,
	models.File.mmos_task_id == models.Solution.mmos_task_id
).with_entities(
	# it's supposed to be pure json, but accidentally stored as String. Will be fixed eventually.
	models.Solution.result, 
	models.File.minmax
).limit(100).all() # @ATTENTION: Take out limit - it's here just for demo purposes

# models.File.minmax defines the conversion from image (scale for the game) to original coordinates and stored as a String
# in the fomat "minX,minY,maxX,maxY"

for result, minmax in solutions:
	_minmax = np.array(json.loads("[%s]" % minmax))
	polygons = json.loads(result)["polygons"]
	orig_polygons = [
		helpers.scale_vertices_to_origin(np.array(poly["vertices"]), _minmax)
		for poly in polygons
	]
	print(orig_polygons)
	# Do something with orig_polygons
	break


# ALWAYS CLOSE SESSION (it takes out the locks if any)
db.close()
```


## Extract solutions from 500th export based on confirmed bots
```python
from fcs_ccp_analysis import db, models

session = db.get_session()

query = session.query(
	models.Solution
).filter(
	models.Solution.classification_flag == 500 # 
)

total_solutions = query.count()

bot_solution_count = query.join(
	models.PlayerProfile,
	models.PlayerProfile.player_id == models.Solution.player_id
).count()

print("{}/{} are bots".format(bot_solution_count, total_solutions))

# ALWAYS CLOSE SESSION (it takes out the locks if any)
db.close()
```

## Database Listing

### models.Batch

```python
id = String(120), PK
task_count = Integer
file_name = String(200)
s3_uploaded = Boolean
mmos_uploaded = Boolean
mmos_synced = Boolean
created_at = DateTime
priority = Integer
maximum_classifications = Integer
```

### models.File

```python
id = String(1000), PK
source_filepath = String(1000)
source_gatepath = String(1000)
filepath = String(1000)
gates = JSON
original_file_id = String(50)
gen_rules_applied = Boolean
batch_id = String(50), FK->'models.Batch.id' # No constraint though
is_gold = Boolean
image_name = String(1000)
need_review = Boolean
under_review = Boolean
reviewed_by = Integer
review_start_at = DateTime
trashed = Boolean
minmax = Column('minmax', String)
difficulty = Integer
peaks = Column('peaks', JSON)
fake_real = Boolean
tag = Column('tag', String)
priority = Integer
mmos_task_id = String(50)
color_scheme = Integer # 1 is old and wrong, 2 is non normalized but n**3, 3 is new compared with R kern-smooth
```

### models.PuzzleResult
```python
id = Integer, PK
mmos_task_id = String(50)
classification_count = Integer
skips = Integer
high_score = Float
```

### models.Solution

```python
id = Integer, PK
mmos_task_id = String(50)
run = Integer
player_id = String(30)
player_project_score = Float
result = JSON
created_at = DateTime
time = Integer
classification_flag = Integer
```

### models.PlayerProfile

```python
player_id = Integer, PK
covered_count = Integer
uncovered_count = Integer
first_seen_flag = Integer
bot_proba = Float
```


## Revisions

### PhyloX Analysis v.0.0.3

Added support for the alternative processed file locations (file.get_file_path__bk with PROCESSED_FOLDER__BK env var).

### PhyloX Analysis v.0.0.2

Added player_profile

### PhyloX Analysis v.0.0.1

Created complete structure of the db in the sqlalchemy terms
