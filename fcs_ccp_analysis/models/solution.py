from sqlalchemy import Column, Integer, String, Boolean, DateTime, Float
from sqlalchemy.dialects.postgresql import JSON
from ._helper import add_prefix
from ._base import Base


class Solution(Base):
    __tablename__ = add_prefix('solution')

    id = Column('id', Integer, primary_key=True)
    mmos_task_id = Column('mmos_task_id', String(50), index=True)

    run = Column('run', Integer, default=1)
    player_id = Column('player_id', String(30))
    player_project_score = Column(
        'player_project_score', Float, default=0)
    result = Column('result', JSON)
    created_at = Column('created_at', DateTime, nullable=True)
    time = Column('time', Integer, default=-1)

    classification_flag = Column(
        'classification_flag', Integer, default=-1, index=True)

    def __repr__(self):
        return '<Solution {}>'.format(self.id)
