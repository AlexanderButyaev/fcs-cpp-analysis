from sqlalchemy import Column, Integer, String, Float
from ._helper import add_prefix
from ._base import Base


class PlayerProfile(Base):
    __tablename__ = add_prefix('player_profile')

    player_id = Column('player_id', String(20), primary_key=True)
    covered_count = Column('covered_count', Integer, default=0)
    uncovered_count = Column('uncovered_count', Integer, default=0)
    first_seen_flag = Column('first_seen_flag', Integer, default=-1)
    bot_proba = Column('bot_proba', Float, default=0.5)

    def __repr__(self):
        return '<PlayerProfile {}>'.format(self.player_id)
