def add_prefix(entity):
    return 'fc_' + entity


def add_prefix_db_index(table_name, *columns):
    return 'ix_%s__%s' % (table_name, '__'.join(columns))
