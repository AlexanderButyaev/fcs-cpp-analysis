import os
from datetime import datetime
from sqlalchemy import Column, Integer, String, Boolean, DateTime
from sqlalchemy.dialects.postgresql import JSON
from ._helper import add_prefix
from ._base import Base


class File(Base):
    __tablename__ = add_prefix('file')

    id = Column('id', String(120), primary_key=True)
    source_filepath = Column(
        'source_filepath', String(1000), nullable=True)  # experiment/_data_/filepath in source_folder
    source_gatepath = Column(
        'source_gatepath', String(1000), nullable=True)  # experiment/_clrs_/filepath in source_folder

    filepath = Column('filepath', String(
        1000), nullable=True)  # experiment/filepath (without .npy/.gate/.dens)

    gates = Column('gates', JSON)

    original_file_id = Column(
        'original_file_id', String(50), nullable=True)
    #
    gen_rules_applied = Column(
        'gen_rules_applied', Boolean, default=False)
    # store rules file - how it was generated from the original file?
    batch_id = Column('batch_id', String(50), nullable=True)

    # image related fields
    is_gold = Column('is_gold', Boolean, default=False)
    image_name = Column('image_name', String(1000), nullable=True)

    need_review = Column('need_review', Boolean, default=True)
    under_review = Column('under_review', Boolean, default=False)
    reviewed_by = Column('reviewed_by', Integer, nullable=True)
    review_start_at = Column('review_start_at', DateTime, nullable=True)
    trashed = Column('trashed', Boolean, default=False)

    minmax = Column('minmax', String)
    difficulty = Column('difficulty', Integer, default=1)
    peaks = Column('peaks', JSON)
    fake_real = Column('fake_real', Boolean, default=False)
    tag = Column('tag', String)
    priority = Column('priority', Integer, default=5)

    # 1 is old and wrong, 2 is non normalized but n**3, 3 is new compared with R kern-smooth
    color_scheme = Column('color_scheme', Integer, default=3)

    mmos_task_id = Column('mmos_task_id', String(50), unique=True)

    def __repr__(self):
        return '<File {}>'.format(self.id)

    @staticmethod
    def get_config_file_path(file_name, config_folder="PROCESSED_FOLDER"):
        return os.path.join(os.getenv(config_folder), file_name)

    @staticmethod
    def get_config_file_path__bk(file_name, config_folder="PROCESSED_FOLDER__BK"):
        return File.get_config_file_path(file_name, config_folder)

    @staticmethod
    def extract_filename_from_path(filepath):
        return os.path.split(filepath)[-1]

    def get_file_path(self):
        return File.get_config_file_path(self.filepath, "PROCESSED_FOLDER")

    def get_file_path__bk(self):
        return File.get_config_file_path__bk(self.filepath, "PROCESSED_FOLDER__BK")

    @staticmethod
    def get_image_config_file_path(file_name, is_gold=False):
        config_folder = "GOLD_RESULT_FOLDER" if is_gold else "REGULAR_RESULT_FOLDER"
        return File.get_config_file_path(file_name, config_folder)

    def get_image_file_path(self):
        return File.get_image_config_file_path(self.image_name, is_gold=self.is_gold)
