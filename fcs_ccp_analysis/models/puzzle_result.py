from sqlalchemy import Column, Integer, String, Boolean, Float, ForeignKey
from ._helper import add_prefix
from ._base import Base


class PuzzleResult(Base):
    __tablename__ = add_prefix('puzzle_result')

    id = Column('id', Integer, primary_key=True)
    mmos_task_id = Column('mmos_task_id', String(50), index=True)
    classification_count = Column(
        'classification_count', Integer, default=0)
    skips = Column('skips', Integer, default=0)
    high_score = Column('high_score', Float, default=0)

    def __repr__(self):
        return '<PuzzleResult task:{}>'.format(self.mmos_task_id)
