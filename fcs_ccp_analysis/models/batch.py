import os
from datetime import datetime
from sqlalchemy import Column, Integer, String, Boolean, DateTime
from ._helper import add_prefix
from ._base import Base


class Batch(Base):
    __tablename__ = add_prefix('batch')

    id = Column('id', String(120), primary_key=True)
    task_count = Column('task_count', Integer, default=0)
    file_name = Column('file_name', String(200))

    s3_uploaded = Column('s3_uploaded', Boolean, default=False)
    mmos_uploaded = Column('mmos_uploaded', Boolean, default=False)
    mmos_synced = Column('mmos_synced', Boolean, default=False)
    created_at = Column('created_at', DateTime,
                           nullable=False, default=datetime.utcnow)

    priority = Column('priority', Integer, default=5)
    maximum_classifications = Column('maximum_classifications', Integer)



    def __repr__(self):
        return '<Batch {}>'.format(self.id)
    
    @staticmethod
    def get_config_file_path(file_name, config_folder="BATCH_EXPORT_FOLDER"):
        return os.path.join(os.getenv(config_folder), file_name)

    @staticmethod
    def extract_filename_from_path(filepath):
        return os.path.split(filepath)[-1]
    
    def get_file_path(self):
        return Batch.get_config_file_path(self.file_name, "BATCH_EXPORT_FOLDER")


    