import os
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session


class DB:
    def __init__(self):
        self.engine = create_engine(
            os.getenv("POSTGRESQL_DATABASE_URL"))
        self.session_factory = sessionmaker(bind=self.engine)
        self.session = scoped_session(self.session_factory)
        print("CREATE NEW THING!!!")

    def get_session(self):
        return self.session()

    def close(self):
        self.session.remove()

    def renew_session(self):
        self.close()
        return self.get_session()
