def scale_vertices_to_img(data, minmax, a=0, b=1):
    mins = minmax[:2]
    maxs = minmax[2:]
    return a + (data - mins) / (maxs - mins) * (b - a)


def scale_vertices_to_origin(data, minmax, a=0, b=1):
    mins = minmax[:2]
    maxs = minmax[2:]
    return mins + (data - a) / (b - a) * (maxs - mins)
