import setuptools

long_description = 'FCS-CCP library that enables user to connect to the FCS-CCP DB and manipulate (directly or through API) DB objects.'

setuptools.setup(
    name="fcs_ccp_analysis",
    version="0.0.3",
    description='FCS-CCP library',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/AlexanderButyaev/fcs-cpp-analysis/src",
    packages=setuptools.find_packages(),
    install_requires=[
                "psycopg2-binary",
                "SQLAlchemy"
    ],
    python_requires='>=3',
    include_package_data=True,
    entry_points={
    },
    classifiers=(
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ),
)
